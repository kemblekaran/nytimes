package io.github.kemblekaran.nytimes.ui.base;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import io.github.kemblekaran.nytimes.utils.CommonUtils;
import io.github.kemblekaran.nytimes.utils.NetworkUtils;

/**
 * Created by Karan Kemble on 28-Feb-18.
 */

public abstract class BaseActivity extends AppCompatActivity implements MvpView {

    private ProgressDialog mProgressDialog;

    @Override
    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    public void showMessage(String message) {
        showToast(message);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onNetworkUnavailable() {

    }

    @Override
    public void onNetworkAvailable() {

    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    protected void showToast(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG);
    }
}
