package io.github.kemblekaran.nytimes.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import io.github.kemblekaran.nytimes.R;
import io.github.kemblekaran.nytimes.ui.base.BaseActivity;
import io.github.kemblekaran.nytimes.ui.topstories.TopStoriesActivity;

public class SplashActivity extends BaseActivity implements SplashMvpView {

    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SplashActivity.this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash);
        openTopStoriesActivity();
    }

    @Override
    public void openTopStoriesActivity() {
         /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the TopStoriesActivity. */
                Intent topStoriesIntent = new Intent(SplashActivity.this, TopStoriesActivity.class);
                SplashActivity.this.startActivity(topStoriesIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}

