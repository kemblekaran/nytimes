package io.github.kemblekaran.nytimes.ui.base;

/**
 * Created by Karan Kemble on 28-Feb-18.
 */

public interface MvpView {
    void showLoading();

    void hideLoading();

    void showMessage(String message);

    void onError();

    void onNetworkUnavailable();

    void onNetworkAvailable();

    boolean isNetworkConnected();
}
