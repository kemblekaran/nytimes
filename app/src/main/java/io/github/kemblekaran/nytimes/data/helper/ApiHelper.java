package io.github.kemblekaran.nytimes.data.helper;

/**
 * Created by Karan Kemble on 28-Feb-18.
 */

public interface ApiHelper {
    ApiHelper getApiHelper();
}
