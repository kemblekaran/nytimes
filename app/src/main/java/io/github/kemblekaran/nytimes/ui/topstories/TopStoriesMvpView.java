package io.github.kemblekaran.nytimes.ui.topstories;

import io.github.kemblekaran.nytimes.ui.base.MvpView;

/**
 * Created by Karan Kemble on 28-Feb-18.
 */

public interface TopStoriesMvpView extends MvpView{
    void attachRecyclerView();
    void initializeAdater();
    void inflateView();
    void refreshNews();
}
