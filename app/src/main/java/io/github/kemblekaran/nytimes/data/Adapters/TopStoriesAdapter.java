package io.github.kemblekaran.nytimes.data.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.github.kemblekaran.nytimes.R;
import io.github.kemblekaran.nytimes.data.model.News;
import io.github.kemblekaran.nytimes.ui.webview.NewsViewActivity;

/**
 * Created by kanDy on 26-02-2018.
 */

public class TopStoriesAdapter extends RecyclerView.Adapter<TopStoriesAdapter.TopStoriesViewHolder> {
    Context mContext;
    String newsURL = "";
    private List<News> topStoriesList;
    private NewsCardOnClickHandler mNewsCardOnClickHandler;

    public TopStoriesAdapter(Context context,List<News> topStoriesList, NewsCardOnClickHandler clickHandler) {
        this.mContext = context;
        this.topStoriesList = topStoriesList;
        this.mNewsCardOnClickHandler = clickHandler;
    }

    @Override
    public TopStoriesAdapter.TopStoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View topStoriesView = LayoutInflater.from(parent.getContext()).inflate(R.layout.top_stories_items, parent, false);
        return new TopStoriesViewHolder(topStoriesView);
    }

    @Override
    public void onBindViewHolder(TopStoriesViewHolder holder, int position) {
        holder.newsTitle.setText(topStoriesList.get(position).getTitle());
        holder.publishedBy.setText(topStoriesList.get(position).getByLine());
    }

    @Override
    public int getItemCount() {
        return topStoriesList.size();
    }

    public interface NewsCardOnClickHandler {
        public void onClick();
    }

    public class TopStoriesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final CardView newsCards;
        public final TextView newsTitle;
        public final TextView publishedBy;

        public TopStoriesViewHolder(View itemView) {
            super(itemView);
            newsCards = itemView.findViewById(R.id.cv_news_card);
            newsTitle = itemView.findViewById(R.id.tv_news_title);
            publishedBy = itemView.findViewById(R.id.tv_published_by);

            newsCards.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            newsURL = topStoriesList.get(getAdapterPosition()).getUrl();
            getWebViewIntent(newsURL);
        }
    }

    public void getWebViewIntent(String newsURL){
        Intent webViewIntent = new Intent(mContext, NewsViewActivity.class);
        webViewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        webViewIntent.putExtra("url", newsURL);
        mContext.startActivity(webViewIntent);
    }
}
