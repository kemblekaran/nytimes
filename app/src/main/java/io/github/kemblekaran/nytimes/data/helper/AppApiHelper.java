package io.github.kemblekaran.nytimes.data.helper;

import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import io.github.kemblekaran.nytimes.utils.AppConstants;

/**
 * Created by kanDy on 27-02-2018.
 */

public class AppApiHelper implements ApiHelper{
    private static final String TAG = "AppApiHelper";


    public static URL buildURL() {
        Uri builtUri = Uri.parse(AppConstants.SERVER_URL)
                .buildUpon()
                .appendQueryParameter(AppConstants.PARAM_QUERY, AppConstants.API_KEY)
                .build();
        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public static String getResponseFromHttpUrl(URL url) throws IOException {


        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = httpURLConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            httpURLConnection.disconnect();
        }
    }

    @Override
    public ApiHelper getApiHelper() {
        return null;
    }
}
