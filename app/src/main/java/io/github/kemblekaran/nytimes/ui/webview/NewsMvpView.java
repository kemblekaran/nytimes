package io.github.kemblekaran.nytimes.ui.webview;

/**
 * Created by Karan Kemble on 28-Feb-18.
 */

public interface NewsMvpView {
    void displayNewsPage();
}
