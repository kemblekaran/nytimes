package io.github.kemblekaran.nytimes.data.asynctask;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.github.kemblekaran.nytimes.data.Adapters.TopStoriesAdapter;
import io.github.kemblekaran.nytimes.data.helper.AppApiHelper;
import io.github.kemblekaran.nytimes.data.model.News;
import io.github.kemblekaran.nytimes.ui.base.MvpView;
import io.github.kemblekaran.nytimes.ui.topstories.TopStoriesActivity;
import io.github.kemblekaran.nytimes.utils.CommonUtils;
import io.github.kemblekaran.nytimes.utils.NetworkUtils;


/**
 * Created by kanDy on 27-02-2018.
 */

public class NetworkRequestTask extends AsyncTask<Void, Void, String> implements MvpView {
    private static final String TAG = "NetworkRequestTask";
    List<News> topStoriesList;
    Context mContext;
    private RecyclerView mTopStoriesRecyclerView;
    private RecyclerView.Adapter mTopStoriesAdapter;
    private TopStoriesAdapter.NewsCardOnClickHandler mNewsCardOnClickHandler;
    URL url = null;

    String title = "";
    String byLine = "";
    String publishedDate = "";
    String newsURL = "";
    ProgressDialog mProgressDialog;

    @Inject
    public NetworkRequestTask(Context context, RecyclerView mTopStoriesRecyclerView, RecyclerView.Adapter mTopStoriesAdapter) {
        super();
        this.mContext = context;
        this.mTopStoriesRecyclerView = mTopStoriesRecyclerView;
        this.mTopStoriesAdapter = mTopStoriesAdapter;
        Log.d(TAG, "NetworkRequestTask: called");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        showLoading();

    }

    @Override
    protected String doInBackground(Void... voids) {
        String response = "";
        try {
            if (isNetworkConnected()) {
                response = AppApiHelper.getResponseFromHttpUrl(AppApiHelper.buildURL()); //make Http Request to the server
            } else {
                onNetworkUnavailable();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        Log.d(TAG, "doInBackground: Response --> " + response);

        try {

            topStoriesList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("results");

            if (jsonObject.get("status").equals("OK")) {
                hideLoading();

                //retrieve top stories from NYTimes API
                for (int i = 0; i < jsonArray.length(); i++) {
                    News news = new News();
                    title = jsonArray.getJSONObject(i).getString("title");
                    byLine = jsonArray.getJSONObject(i).getString("byline");
                    publishedDate = jsonArray.getJSONObject(i).getString("published_date");
                    newsURL = jsonArray.getJSONObject(i).getString("url");

                    news.setTitle(title);
                    news.setByLine(byLine);
                    news.setPublishedDate(publishedDate);
                    news.setUrl(newsURL);

                    topStoriesList.add(news);
                }


                mTopStoriesAdapter = new TopStoriesAdapter(mContext, topStoriesList, mNewsCardOnClickHandler);
                mTopStoriesRecyclerView.setAdapter(mTopStoriesAdapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoading() {
        mProgressDialog = CommonUtils.showLoadingDialog(mContext);
    }

    @Override
    public void hideLoading() {
        mProgressDialog.cancel();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onNetworkUnavailable() {
        Intent topStoriesIntent = new Intent(mContext, TopStoriesActivity.class);
        mContext.startActivity(topStoriesIntent);
        ((Activity) mContext).finish();
    }

    @Override
    public void onNetworkAvailable() {

    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(mContext);
    }
}
