package io.github.kemblekaran.nytimes.ui.splash;

import io.github.kemblekaran.nytimes.ui.base.MvpView;

/**
 * Created by Karan Kemble on 28-Feb-18.
 */

public interface SplashMvpView extends MvpView {
    void openTopStoriesActivity();
}
