package io.github.kemblekaran.nytimes.ui.webview;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import io.github.kemblekaran.nytimes.R;
import io.github.kemblekaran.nytimes.ui.base.BaseActivity;
import io.github.kemblekaran.nytimes.ui.base.MvpView;
import io.github.kemblekaran.nytimes.utils.CommonUtils;

public class NewsViewActivity extends BaseActivity implements NewsMvpView {
    private WebView mNewsView;
    private TextView newsNetworkWarning;
    private ImageView newsNetworkImage;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        newsNetworkWarning = findViewById(R.id.tv_news_network_warning);
        newsNetworkImage = findViewById(R.id.iv_news_network_gone);
        mNewsView = findViewById(R.id.wv_news);

        Intent intent = getIntent();
        String url = intent.getStringExtra("url");


        if (isNetworkConnected()) {
            showLoading();
            displayNewsPage();
            mNewsView.loadUrl(url);
        } else {
            onNetworkUnavailable();
        }


    }

    @Override
    public void showLoading() {
        mProgressDialog = CommonUtils.showLoadingDialog(NewsViewActivity.this);
    }

    @Override
    public void hideLoading() {
        mProgressDialog.cancel();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onNetworkUnavailable() {
        mNewsView.setVisibility(View.GONE);
        newsNetworkWarning.setVisibility(View.VISIBLE);
        newsNetworkImage.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNetworkAvailable() {
        newsNetworkWarning.setVisibility(View.GONE);
        newsNetworkImage.setVisibility(View.GONE);
        mNewsView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean isNetworkConnected() {
        return super.isNetworkConnected();
    }

    @Override
    public void displayNewsPage() {
        onNetworkAvailable();
        mNewsView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(NewsViewActivity.this, description, Toast.LENGTH_SHORT).show();
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (mProgressDialog.isShowing()) {
                    hideLoading();
                }
            }


        });
    }
}


