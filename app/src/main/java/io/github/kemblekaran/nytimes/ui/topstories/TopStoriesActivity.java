package io.github.kemblekaran.nytimes.ui.topstories;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.github.kemblekaran.nytimes.R;
import io.github.kemblekaran.nytimes.data.asynctask.NetworkRequestTask;
import io.github.kemblekaran.nytimes.data.model.News;
import io.github.kemblekaran.nytimes.ui.base.BaseActivity;


public class TopStoriesActivity extends BaseActivity implements TopStoriesMvpView {
    private RecyclerView mTopStoriesRecyclerView;
    private RecyclerView.Adapter mTopStoriesAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<News> topStoriesList;
    private TextView networkMessage;
    private ImageView networkGone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_stories);

        networkMessage = findViewById(R.id.tv_network_warning);
        networkGone = findViewById(R.id.iv_network_gone);
        mTopStoriesRecyclerView = findViewById(R.id.rv_top_stories);
        topStoriesList = new ArrayList<>();

        inflateView();

    }

    @Override
    protected void onResume() {
        super.onResume();
//        inflateView();
    }

    @Override
    public void inflateView() {
        if(isNetworkConnected()){
            onNetworkAvailable();
            attachRecyclerView();
            initializeAdater();
        }else{
           onNetworkUnavailable();
        }
    }

    @Override
    public void attachRecyclerView() {
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mTopStoriesRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mTopStoriesRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void initializeAdater() {
        NetworkRequestTask networkRequestTask = new NetworkRequestTask(TopStoriesActivity.this, mTopStoriesRecyclerView, mTopStoriesAdapter);
        networkRequestTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void refreshNews() {
        inflateView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refreshNews();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onNetworkUnavailable() {
        mTopStoriesRecyclerView.setVisibility(View.GONE);
        networkMessage.setVisibility(View.VISIBLE);
        networkGone.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNetworkAvailable() {
        mTopStoriesRecyclerView.setVisibility(View.VISIBLE);
        networkMessage.setVisibility(View.GONE);
        networkGone.setVisibility(View.GONE);
    }

    @Override
    public boolean isNetworkConnected() {
        return super.isNetworkConnected();
    }


}
